Local OpenAI Bot
================

OpenAI bot to run locally as a python web app

Configuration
-------------
1. install Conda
2. configure environment with command in the project root
```console
    conda env create -f environment.yml -y
```
3. activate the created environment with command
```console
    conda activate local-openai-bot
```
4. create file openai-api.key with OpenAI API KEY

Start
-----

    python main.py

Usage
-----

- The bot handles multiple named sessions which can be created/updated/removed.
- Every session has optional system prompt.
- By default session sends just user message with system prompt if it is provided in the session settings.
- If the message must contain previous messages (for context request) the amount of previous message pairs (user+assistent) must be specified with selected 