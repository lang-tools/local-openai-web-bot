from fastapi import FastAPI, Form, Path, HTTPException, status
import os
from fastapi.requests import Request
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

from loguru import logger

from config import Config, get_config
from openai_bot import OpenAIBot

config:Config = get_config()
with open(config.openaiApiKeyFile, 'r') as fin:
    os.environ['OPENAI_API_KEY'] = fin.read()
    logger.info(f'configured OPENAI_API_KEY from file "{config.openaiApiKeyFile}"')

appVersion:str|None = None

def get_app_version() -> str:
    global appVersion
    if appVersion is None:
        with open('VERSION', 'r') as fin:
            appVersion = fin.read()
    return appVersion

app = FastAPI(
    title='Deutch AI tutor',
    description='Deutch AI Tutor pages',
    version=get_app_version()
)

app.mount("/css", StaticFiles(directory="css"), name="css")
templates = Jinja2Templates('templates')

@app.get("/", response_class=HTMLResponse)
def get_home(request:Request):
    chats = bot.get_chats_list()
    return templates.TemplateResponse(
        'home.html',
        dict(
            request=request,
            chats=chats
        ))

bot = OpenAIBot(chatsDir=config.chatsDir)
logger.info(f'chats dir set to be "{config.chatsDir}"')

@app.get('/chat/create', response_class=HTMLResponse)
def create_chat(request:Request):
    return templates.TemplateResponse(
        'chat-create.html',
        dict(
            request=request
        ))

@app.post('/chat/create')
def do_chat_create(name:str=Form(), systemPrompt:str=Form(default=None)):
    bot.create_chat(name, systemPrompt)
    return RedirectResponse(
        url=f'/chat/view/{name}',
        status_code=status.HTTP_302_FOUND)

@app.get('/chat/view/{name}', response_class=HTMLResponse)
def chat_view(request:Request, name:str=Path()):
    chat = bot.get_chat(name)
    if chat is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Chat does not exists')
    return templates.TemplateResponse(
        'chat-view.html',
        dict(
            request=request,
            name=name,
            chat=chat
        ))

@app.post('/chat/view/{name}', response_class=HTMLResponse)
def do_chat(name:str=Path(), message:str=Form(default=''), contextSize:int=Form(0)):
    print(contextSize)
    print(message)
    chat = bot.get_chat(name)
    if chat is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Chat does not exists')
    bot.send_message(name, message, contextSize)
    return RedirectResponse(
        url=f'/chat/view/{name}',
        status_code=status.HTTP_302_FOUND)

@app.get('/chat/edit/{name}', response_class=HTMLResponse)
def chat_edit(request:Request, name:str=Path()):
    chat = bot.get_chat(name)
    if chat is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Chat does not exists')
    return templates.TemplateResponse(
        'chat-edit.html',
        dict(
            request=request,
            chat=chat
        ))

@app.post('/chat/edit/{name}')
def do_chat_edit(name:str=Path(), systemPrompt:str=Form(default=None)):
    bot.update_chat(name, systemPrompt)
    return RedirectResponse(
        url=f'/chat/view/{name}',
        status_code=status.HTTP_302_FOUND)

@app.get('/chat/delete/{name}', response_class=HTMLResponse)
def chat_delete(request:Request, name:str=Path()):
    chat = bot.get_chat(name)
    if chat is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Chat does not exists')
    return templates.TemplateResponse(
        'chat-delete.html',
        dict(
            request=request,
            chat=chat
        ))

@app.post('/chat/delete/{name}')
def do_chat_delete(name:str=Path()):
    bot.remove_chat(name)
    return RedirectResponse(
        url=f'/',
        status_code=status.HTTP_302_FOUND)

@app.get("/version", description='Versions info')
def get_version() -> dict[str, str]:
    return dict(
        appVersion=get_app_version()
    )

if __name__ =='__main__':
    import uvicorn
    logger.info(f'running with port {config.port}')
    if config.development:
        uvicorn.run(app='main:app', port=config.port, reload=True)
    else:
        uvicorn.run(app=app, port=config.port)