import glob
import  os
from pydantic import BaseModel, validator
from openai import OpenAI
from strictyaml import YAML
from strictyaml import load as load_yaml

from loguru import logger

class Message(BaseModel):
    role:str
    content:str
    
    def to_dict(self) -> dict[str, str]:
        return dict(role=self.role, content=self.content)

class Chat(BaseModel):
    name: str
    systemPrompt: str = ''
    messages: list[Message]|str = ''

    def get_messages(self) -> list[Message]:
        if isinstance(self.messages, str):
            return []
        return self.messages
    
    def set_messages(self, messages:list[Message]):
        if len(messages) == 0:
            self.messages = ''
        else:
            self.messages = messages
    
    @validator('messages', pre=True)
    def prepare_messages(cls, value):
        if isinstance(value, str):
            return []
        return value
    
    def to_dict(self):
        return dict(
            name=self.name,
            systemPrompt=self.systemPrompt if self.systemPrompt else '',
            messages=[m.to_dict() for m in self.messages] if len(self.messages) > 0 else ''
        )

class OpenAIBot:
    def __init__(self, model='gpt-3.5-turbo', chatsDir:str='chats'):
        self.chatsDir=chatsDir
        self.model = model
        self.client = OpenAI()

    def send_message(self, chatName:str, message:str, contextSize:int=0) -> Chat:
        """
        contextSize - amount of conversation rounds to include in the prompt"""
        chat = self.get_chat(chatName)
        # print(chat)
        contextMessages:list = []

        if contextSize > 0 and len(chat.messages) > 0:
            contextMessages = chat.get_messages()[-(contextSize*2):]
        if chat.systemPrompt:
            contextMessages.insert(0, Message(role="system", content=chat.systemPrompt))
        messages = [ contextMessage.to_dict() for contextMessage in contextMessages ]
        newMessage=Message(role='user', content=message)
        chatMessages = chat.get_messages()
        chatMessages.append(newMessage)
        messages.append(newMessage.to_dict())
        # print(messages)
        logger.debug('chat model = "{self.model}", messages:')
        logger.debug(messages)
        response = self.client.chat.completions.create(
            model=self.model,
            messages=messages
        )
        resMessage = response.choices[0].message
        logger.debug('model response:')
        logger.debug(resMessage)
        content = resMessage.content if isinstance(resMessage.content, str) else ''
        chatMessages.append(Message(role=resMessage.role, content=content))
        chat.set_messages(chatMessages)
        data = YAML(chat.to_dict())
        chatFile = f'{self.chatsDir}/{chatName}.yml'
        with open(chatFile, 'w') as fout:
            fout.write(data.as_yaml())
        return chat
    
    def create_chat(self, chatName:str, systemPrompt:str|None=None) -> list[Message]:
        chatFile = f'{self.chatsDir}/{chatName}.yml'
        if os.path.exists(chatFile):
            raise ValueError('chat already exist')
        chat = Chat(name=chatName,
            systemPrompt=systemPrompt if systemPrompt else '',
            messages=''
        )
        data = YAML(chat.to_dict())
        with open(chatFile, 'w') as fout:
            fout.write(data.as_yaml())
        return []
    
    def get_chats_list(self) -> list[str]:
        chatFiles = glob.glob(f'{self.chatsDir}/*.yml')
        res = []
        for f in chatFiles:
            res.append(f[len(self.chatsDir) + 1: -4])
        return res
    
    def get_chat(self, name:str) -> Chat:
        """Returns the chat if it exists.
         
          If the chat is not know - raises ValueError exception."""
        chatFile = f'{self.chatsDir}/{name}.yml'
        if not os.path.exists(chatFile):
            raise ValueError(f'chat "{name}" does not exist')
        with open(chatFile, 'r') as fin:
            data = fin.read()
            chatData = Chat(
                **(load_yaml(data).data)
            )
            if isinstance(chatData.messages, str):
                chatData.messages = []
            return chatData
    
    def remove_chat(self, chatName:str) -> None:
        chatFile = f'{self.chatsDir}/{chatName}.yml'
        if os.path.exists(chatFile):
            os.remove(chatFile)


    def update_chat(self, chatName:str, systemPrompt:str) -> None:
        chat = self.get_chat(chatName)
        chat.systemPrompt = systemPrompt
        chatFile = f'{self.chatsDir}/{chatName}.yml'
        chat.systemPrompt = systemPrompt if systemPrompt else ''
        if len(chat.messages) == 0:
            chat.messages = ''
        data = YAML(chat.to_dict())
        with open(chatFile, 'w') as fout:
            fout.write(data.as_yaml())
    