from pydantic import BaseModel
from strictyaml import load as load_yaml

class Config(BaseModel):
    development: bool
    port:int
    chatsDir:str
    openaiApiKeyFile:str

_config:Config|None = None

def get_config() ->  Config:
    global _config
    if _config is None:
        with open('config.yml', 'r') as fin:
            _config = Config(**load_yaml(fin.read()).data)
    return _config